<?php

$options = [
    '.env' => '--env',
    'browser.config' => '--browser',
    'http.config' => '--http',
    'ssl.config' => '--ssl',
];

$args = $argv ?? [];
$selected = array_intersect($options, $args);
if (count($selected) === 0) {
    print "Argument argument required: " .  PHP_EOL . implode(PHP_EOL, $options) . PHP_EOL;
    return;
}

$outputDir = dirname(__FILE__) . '/output';
foreach ($selected as $fileName => $select) {
    $file = $outputDir . DIRECTORY_SEPARATOR . $fileName;
    $fp = fopen($file, "wb");
    fwrite($fp, 'Sample');
    fclose($fp);
    print "$fileName created!" . PHP_EOL;
}
