<?php

/**
 * Laravel Requirement Checker
 *
 * This standalone script will check if your server meets the requirements for running the
 * Laravel web application framework.
 *
 * @author Gastón Heim
 * @author Emerson Carvalho
 * @version 0.0.1
 * @source https://github.com/GastonHeim/Laravel-Requirement-Checker/blob/master/check.php
 */


$latestLaravelVersion = '8.0';
$laravelVersion = (string) isset($argv[1]) ? $argv[1] : $latestLaravelVersion;

if (!in_array($laravelVersion, array('4.2', '5.0', '5.1', '5.2', '5.3', '5.4', '5.5', '5.6', '5.7', '5.8', '6.0', '7.0'))) {
    $laravelVersion = $latestLaravelVersion;
}

$laravel42Obs = 'As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension.
When using Ubuntu, this can be done via apt-get install php5-json.';
$laravel50Obs = 'PHP version should be < 7. As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension.
When using Ubuntu, this can be done via apt-get install php5-json';

$reqList = array(
    '4.2' => array(
        'php' => '5.4',
        'mcrypt' => true,
        'pdo' => false,
        'openssl' => false,
        'mbstring' => false,
        'tokenizer' => false,
        'xml' => false,
        'ctype' => false,
        'json' => false,
        'obs' => $laravel42Obs
    ),
    '5.0' => array(
        'php' => '5.4',
        'mcrypt' => true,
        'openssl' => true,
        'pdo' => false,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => false,
        'ctype' => false,
        'json' => false,
        'obs' => $laravel50Obs
    ),
    '5.1' => array(
        'php' => '5.5.9',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => false,
        'ctype' => false,
        'json' => false,
        'obs' => ''
    ),
    '5.2' => array(
        'php' => array(
            '>=' => '5.5.9',
            '<' => '7.2.0',
        ),
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => false,
        'ctype' => false,
        'json' => false,
        'obs' => ''
    ),
    '5.3' => array(
        'php' => array(
            '>=' => '5.6.4',
            '<' => '7.2.0',
        ),
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => false,
        'json' => false,
        'obs' => ''
    ),
    '5.4' => array(
        'php' => '5.6.4',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => false,
        'json' => false,
        'obs' => ''
    ),
    '5.5' => array(
        'php' => '7.0.0',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => false,
        'json' => false,
        'obs' => ''
    ),
    '5.6' => array(
        'php' => '7.1.3',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => true,
        'json' => true,
        'obs' => ''
    ),
    '5.7' => array(
        'php' => '7.1.3',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => true,
        'json' => true,
        'obs' => ''
    ),
    '5.8' => array(
        'php' => '7.1.3',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => true,
        'json' => true,
        'obs' => ''
    ),
    '6.0' => array(
        'php' => '7.2.0',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => true,
        'json' => true,
        'bcmath' => true,
        'obs' => ''
    ),
    '7.0' => array(
        'php' => '7.2.5',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => true,
        'json' => true,
        'bcmath' => true,
        'obs' => ''
    ),
    '8.0' => array(
        'php' => '7.3.0',
        'mcrypt' => false,
        'openssl' => true,
        'pdo' => true,
        'mbstring' => true,
        'tokenizer' => true,
        'xml' => true,
        'ctype' => true,
        'json' => true,
        'bcmath' => true,
        'obs' => ''
    ),
);


$strOk = '✓ Passed ';
$strFail = 'X Failed ';
$strUnknown = '?';

$requirements = array();


// PHP Version
if (is_array($reqList[$laravelVersion]['php'])) {
    $requirements['php_version'] = true;
    foreach ($reqList[$laravelVersion]['php'] as $operator => $version) {
        if ( ! version_compare(PHP_VERSION, $version, $operator)) {
            $requirements['php_version'] = false;
            break;
        }
    }
}else{
    $requirements['php_version'] = version_compare(PHP_VERSION, $reqList[$laravelVersion]['php'], ">=");
}

// OpenSSL PHP Extension
$requirements['openssl_enabled'] = extension_loaded("openssl");

// PDO PHP Extension
$requirements['pdo_enabled'] = defined('PDO::ATTR_DRIVER_NAME');

// Mbstring PHP Extension
$requirements['mbstring_enabled'] = extension_loaded("mbstring");

// Tokenizer PHP Extension
$requirements['tokenizer_enabled'] = extension_loaded("tokenizer");

// XML PHP Extension
$requirements['xml_enabled'] = extension_loaded("xml");

// CTYPE PHP Extension
$requirements['ctype_enabled'] = extension_loaded("ctype");

// JSON PHP Extension
$requirements['json_enabled'] = extension_loaded("json");

// Mcrypt
$requirements['mcrypt_enabled'] = extension_loaded("mcrypt_encrypt");

// BCMath
$requirements['bcmath_enabled'] = extension_loaded("bcmath");

// mod_rewrite
$requirements['mod_rewrite_enabled'] = null;

if (function_exists('apache_get_modules')) {
    $requirements['mod_rewrite_enabled'] = in_array('mod_rewrite', apache_get_modules());
}

print "Laravel {$laravelVersion} Server Requirements." . PHP_EOL;
 
print ($requirements['php_version'] ? $strOk : $strFail) . "PHP[" . PHP_VERSION."] "; 

print 'PHP ';
if (is_array($reqList[$laravelVersion]['php'])) {
    $phpVersions = array();
    foreach ($reqList[$laravelVersion]['php'] as $operator => $version) {
        $phpVersions[] = "{$operator} {$version}";
    }
    print implode(" && ", $phpVersions);
} else {
    print ">= " . $reqList[$laravelVersion]['php'];
}

if ($reqList[$laravelVersion]['openssl']) {
    print PHP_EOL . ($requirements['openssl_enabled'] ? $strOk : $strFail) . "OpenSSL PHP Extension";
}

if($reqList[$laravelVersion]['pdo']) {
    print PHP_EOL . ($requirements['pdo_enabled'] ? $strOk : $strFail) . "PDO PHP Extension";
}

if($reqList[$laravelVersion]['mbstring']) {
    print PHP_EOL . ($requirements['mbstring_enabled'] ? $strOk : $strFail) . "Mbstring PHP Extension";
}

if($reqList[$laravelVersion]['tokenizer']) {
    print PHP_EOL . ($requirements['tokenizer_enabled'] ? $strOk : $strFail) . "Tokenizer PHP Extension";
}

if($reqList[$laravelVersion]['xml']) {
    print PHP_EOL . ($requirements['xml_enabled'] ? $strOk : $strFail) . "XML PHP Extension";
}

if($reqList[$laravelVersion]['ctype']) {
    print PHP_EOL . ($requirements['ctype_enabled'] ? $strOk : $strFail) . "CTYPE PHP Extension";
}

if($reqList[$laravelVersion]['json']) {
    print PHP_EOL . ($requirements['json_enabled'] ? $strOk : $strFail) . "JSON PHP Extension";
}

if($reqList[$laravelVersion]['mcrypt']) {
    print PHP_EOL . ($requirements['mcrypt_enabled'] ? $strOk : $strFail) . "Mcrypt PHP Extension";
}

if (isset($reqList[$laravelVersion]['bcmath']) && $reqList[$laravelVersion]['bcmath']) {
    print  PHP_EOL .( $requirements['bcmath_enabled'] ? $strOk : $strFail) . "BCmath PHP Extension";
}

if (!empty($reqList[$laravelVersion]['obs'])) {
    print PHP_EOL . ($reqList[$laravelVersion]['obs'] ? $strOk : $strFail) . "OBS";
}

print PHP_EOL . (!ini_get('magic_quotes_gpc') ? $strOk : $strFail) . "magic_quotes_gpc[".ini_get('magic_quotes_gpc')."]";
print PHP_EOL . (!ini_get('register_globals') ? $strOk : $strFail) . "register_globals[".ini_get('register_globals')."]";
print PHP_EOL . (!ini_get('session.auto_start') ? $strOk : $strFail) . "session.auto_start[".ini_get('session.auto_start')."]";
print PHP_EOL . (!ini_get('mbstring.func_overload') ? $strOk : $strFail) . "mbstring.func_overload[".ini_get('mbstring.func_overload')."]";