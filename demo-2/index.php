<?php
require('../vendor/autoload.php');
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '.env');
$dotenv->load();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Demo 2</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Demo 2</h1>
    <p>Updating .env value</p>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">Environment</th>
        <th scope="col">Value</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($_ENV as $key => $value) : ?>
        <tr>
            <th scope="row"><?=$key?></th>
            <td><?=$value?></td>
        </tr>    
        <?php endforeach;?> 
    </tbody>
    </table>    
</div>
</body>
</html>
