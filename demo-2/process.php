<?php
require('../vendor/autoload.php');
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '.env');
$dotenv->load();

$env = $argv[1] ?? '';

if (!$env) {
    print 'Enter new environment value.' . PHP_EOL;
    return;
}

update_env('APP_ENV', $env);

print "Environment changed to: {$env}" . PHP_EOL;

// replace old file
create_env_file($fileName = '.env');

function update_env($key, $value)
{
    return $_ENV[$key] = $value;
}

function create_env_file($fileName = '.env')
{
    $file = '';
    foreach ($_ENV as $key => $value) {
        $file .= "{$key}={$value}" . PHP_EOL;
    }
    return file_put_contents($fileName, $file) ? $fileName : '';
}
