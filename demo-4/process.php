<?php

// sudo service mariadb status
print 'Restarting application...' . PHP_EOL;
$output = shell_exec("sudo service mariadb restart 2>&1;");

if (!stripos($output, 'done')) {
    print "Error: {$output}";
    exit;
}

print 'Application restarted!' . PHP_EOL;
$output = shell_exec("sudo service mariadb status 2>&1");
print $output;
