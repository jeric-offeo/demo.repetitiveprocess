run `composer install` to download dependency packages

- `demo-1` Get .env values.
- `demo-2` Update .env values.
- `demo-3` Validation checker (e.g. server requirements).
- `demo-4` Restart service application.
- `demo-5` File switching
- `demo-6` Update directory permission
- `demo-7` Automation of file creation.
- `demo-8` Update git repo.
