<?php
require('../vendor/autoload.php');
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '.env');
$dotenv->load();

/**
 * $argv is the default variable used by php to get the arguments
 */

$targetKeys = array_unique(
                    array_filter(explode(',', $argv[1] ?? ''))
                );
if(count($targetKeys) === 0) {
    print "No key provided.\n";
    exit;
}

print "\nEnvironment values:\n\n";
foreach($targetKeys as $key) {    
    if(isset($_ENV[$key])) {
        print "{$key}: {$_ENV[$key]}\n";
    } else {
        print "{$key}: N.A.\n";
    }
}