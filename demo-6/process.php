<?php
// php process.php "root:root" "/var/www/demo.repetitiveprocess/demo-6/sample-folder"

// Adding “ 2>&1 ” to your shell command inform the shell to pipe all responses back to the shell_exec() method.

$owner = $argv[1] ?? 'root:root';
$directory = $argv[2] ?? dirname(__FILE__);
$permission = $argv[3] ?? 'ug+rwx';
$ownerChange = shell_exec("sudo chown -R {$owner} {$directory} 2>&1");
if ($ownerChange) {
    print "Error: {$ownerChange}";
    return;
}

$permissionChange = shell_exec("sudo chmod -R {$permission} $directory 2>&1");
if ($permissionChange) {
    print "Error: {$permissionChange}";
    return;
}
print "Directory: {$directory}" . PHP_EOL;
print "Owner: {$owner}" . PHP_EOL;
print "Permission: {$permission}" . PHP_EOL;
print "Done!" . PHP_EOL;

print '--------------------' . PHP_EOL;

$output = shell_exec("ls {$directory} -l 2>&1");
print $output;
