<?php
require('../vendor/autoload.php');

$environments = ['local', 'production', 'staging'];
$choice = $argv[1] ?? '';

if (!$choice) {
    print "Choose an environment:" . PHP_EOL . implode(PHP_EOL, $environments) . PHP_EOL;
    return;
}
$targetFile = '.env';
$sourceFile = ".env.{$choice}";

if (!file_exists($sourceFile)) {
    print "Can't locate file: {$sourceFile}" . PHP_EOL;
    return;
}

shell_exec("cp {$sourceFile} {$targetFile}");

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '.env');
$dotenv->load();

update_env('TIMESTAMP', '"' . date('Y-m-d H:i:s') . '"');
// replace old file
create_env_file($fileName = '.env');

print "File switched! {$sourceFile} > {$targetFile}" . PHP_EOL;

function update_env($key, $value)
{
    return $_ENV[$key] = $value;
}

function create_env_file($fileName = '.env')
{
    $file = '';
    foreach ($_ENV as $key => $value) {
        $file .= "{$key}={$value}" . PHP_EOL;
    }
    return file_put_contents($fileName, $file) ? $fileName : '';
}
